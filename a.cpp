template<typename T, typename U> struct Pair {};
template<typename... Ts> void f(Ts...);

template<typename... Ts> struct Container 
{
    template<typename... Us> void apply()
    {
        apply(Pair<Ts, Us>()...);
    }
};

void test(Container<int, float> cont) {
    cont.apply<int, float, double>();
}
