#include <vector>
#include <iostream>

int main()
{
    std::vector<int> my_container;
    my_container.push_back(1); my_container.push_back(3); my_container.push_back(3); my_container.push_back(7);

    for (std::vector<int>::const_iterator I = my_container.begin(), E = my_container.end(); I != E; ++I) { std::cout << *I << std::endl; }
}
