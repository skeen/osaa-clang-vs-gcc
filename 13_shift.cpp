#include <iostream>

int shift(int n) {
    return 2 << n;
}

int main() {
    std::cout << shift(42) << std::endl;
}
