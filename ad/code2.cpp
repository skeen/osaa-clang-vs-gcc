template<class T>
void f(T::type) { }
struct A { };

int main()
{
  A a;
  f<A>(a);
}
