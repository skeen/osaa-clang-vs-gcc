int main()
{
    constexpr x = 0;
    constexpr y = 1;
    if(x<y) return 42;
    return 0;
}

